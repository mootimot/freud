// Copyright (c) 2010-2018 The Regents of the University of Michigan
// This file is part of the freud project, released under the BSD 3-Clause License.

#include <iostream>

#include "box.h"
#include "Index1D.h"

using namespace std;

/*! \file box.cc
    \brief Represents simulation boxes and contains helpful wrapping functions.
*/

namespace freud { namespace box {

}; }; // end namespace freud::box
