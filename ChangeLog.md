# Change Log

## v0.7.0

* Various bug fixes and code cleaning
* Fixed all compile-time warnings
* Ensured PEP 8 compliance everywhere
* Minimized boost dependence
* Added Nematic order parameter
* Added optional rmin argument to density.RDF
* Added credits file
* Many documentation rewrites
* Wrote development guide
* Made tests deterministic (seeded RNGs)
* Removed deprecated Box API warnings
* Standardized numpy usage
* Added Python interface for box periodicity

## v0.6.4

* Added a generic neighbor list interface
* Set up CircleCI for continuous integration
* Set up documentation on ReadTheDocs
* Added bumpversion support
* Various bug fixes
* Added python-style properties for accessing data
* Fixed issues with voronoi neighbor list

## v0.6.0

* trajectory module removed
* box constructor API updated
* PMFTXYZ API updated to take in more quaternions for `face_orientations`, or have a sensible default value
* NearestNeighbors:
    - over-expanding box fixed
    - strict rmax mode added
    - ability to get wrapped vectors added
    - minor updates to C-API to return full lists from C
* Addition of Bonding modules
* Addition of local environment matching

## v0.5.0

* Replace boost::shared\_array with std::shared\_ptr (C++ 11)
* Moved all tbb template classes to lambda expressions
* Moved trajectory.Box to box.Box
* trajectory is deprecated
* Fixed Bond Order Diagram and allow for global, local, or orientation correlation
* Added python-level voronoi calculation
* Fixed issues with compiling on OS X, including against conda python installs
* Added code to compute bonds between particles in various coordinate systems

## v0.4.1
* PMFT: Fixed issue involving binning of angles correctly
* PMFT: Fixed issue in R12 which prevented compute/accumulate from being called with non-flattened arrays
* PMFT: Updated xyz api to allow simpler symmetric orientations to be supplied
* PMFT: Updated pmftXY2D api
* PMFT: Histograms are properly normalized, allowing for comparison between systems without needing to "zero" the system
* fsph: Added library to calculate spherical harmonics via cython
* Local Descriptors: Uses fsph, updates to API
* Parallel: Added default behavior to setNumThreads and added context manager


## v0.4.0

* Add compiler flags for C++11 features
* Added Saru RNG (specifically for Cubatic Order Parameter, available to all)
* Cubatic Order Parameter
* Rank 4 tensor struct
* Environment Matching/Cluster Environment
* Shape aware fourier transform for structure factor calculation
* Added deprecation warnings; use python -W once to check warnings
* Added Change Log
* Moved all documentation in Sphinx; documentation improvements
* C++ wrapping moved from Boost to Cython
* Itercell only available in python 3.x
* PMFT:
    - XY
    - XYZ with symmetry, API updated
    - R, T1, T2
    - X, Y, T2
* viz removed (is not compatible with cython)

## No change logs prior to v0.4.0 ##
