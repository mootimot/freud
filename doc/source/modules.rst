=======
Modules
=======

Below is a list of modules in freud. To add your own module, read the
:doc:`development guide </development>`.

.. toctree::
   :maxdepth: 3

   bond
   box
   cluster
   density
   indexer
   interface
   kspace
   locality
   pmft
   order
