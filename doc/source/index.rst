===================
freud documentation
===================

*"Neurosis is the inability to tolerate ambiguity" - Sigmund Freud*

The freud library is a Python package meant for the analysis of molecular dynamics and Monte Carlo
simulation trajectories. The freud library works with and returns `NumPy <http://www.numpy.org/>`_ arrays.

Please visit our repository on `Bitbucket <https://bitbucket.org/glotzer/freud>`_ for the library source code, post
issues or bugs to our `issue tracker <https://bitbucket.org/glotzer/freud/issues?status=new&status=open>`_, and ask
questions and discuss on our `forum <https://groups.google.com/forum/#!forum/freud-users>`_.

Contents
========

.. toctree::
   :maxdepth: 2

   examples
   installation
   modules
   development
   citations
   license
   credits


Index
=====

* :ref:`genindex`
* :ref:`search`
