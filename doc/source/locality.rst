===============
Locality Module
===============

The locality module contains data structures to efficiently locate points based
on their proximity to other points.

NeighborList
============

.. autoclass:: freud.locality.NeighborList
   :members:

LinkCell
========

.. autoclass:: freud.locality.LinkCell(box, cell_width)
   :members:

NearestNeighbors
================

.. autoclass:: freud.locality.NearestNeighbors(rmax, n_neigh, scale=1.1, strict_cut=False)
   :members:
